CREATE DATABASE IF NOT EXISTS Developers;
USE Developers;

CREATE TABLE IF NOT EXISTS Utente (
    NomeUtente VARCHAR(32) PRIMARY KEY NOT NULL, -- Considero il nome utente univoco
    cognome VARCHAR(32) NOT NULL,
    nome VARCHAR(32) NOT NULL,
    cellulare VARCHAR(10) UNIQUE NOT NULL,
    mail VARCHAR(64) UNIQUE NOT NULL,
    password VARCHAR(64) NOT NULL, -- Imposto il numero di caratteri per la password a 64 poiché pensavo di utilizzare un algoritmo di hashing come SHA-256
    ruolo VARCHAR(64) -- Con ruolo intendo se è un grafico o uno sviluppatore, ecc.
);

CREATE TABLE IF NOT EXISTS Skill (
    Nome VARCHAR(64) PRIMARY KEY NOT NULL, -- Il nome della skill sarà univoco, quindi posso considerarlo come pk
    Padre VARCHAR(64) DEFAULT NULL, -- Padre intendo la famiglia di skill dalla quale proviene
    FOREIGN KEY (Padre) REFERENCES Skill(Nome)
);

CREATE TABLE IF NOT EXISTS Progetto (
    IdProgetto INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome VARCHAR(32) DEFAULT "",
    descrizione VARCHAR(255) DEFAULT "",
	IdCoordinatore VARCHAR(32) NOT NULL, -- Ci sarà un coordinatore che è un utente
    FOREIGN KEY (IdCoordinatore) REFERENCES Utente(NomeUtente)
);

CREATE TABLE IF NOT EXISTS Task (
    IdTask INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    dataInizio DATE NOT NULL,
    dataFine DATE, -- Considero di non avere deadline in alcune task, quindi questo campo potrebbe essere ipoteticamente vuoto.
    stato BOOLEAN DEFAULT TRUE NOT NULL, -- TRUE lo intendo come la task è ancora aperta, presumo che quando verrà inserita una task questa sarà ancora da completare
    descrizione VARCHAR(255),
    IdProgetto INT NOT NULL, -- Obbligatoriamente deve essere associato ad un progetto
    FOREIGN KEY (IdProgetto) REFERENCES Progetto(IdProgetto)
);

CREATE TABLE IF NOT EXISTS File (
    IdFile INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    nome VARCHAR(127) NOT NULL,
    descrizione VARCHAR(255) NOT NULL,
    filePath VARCHAR(255) NOT NULL, -- Mi salvo il path di dove andrò a salvare il file così da poterlo trovare all'interno dei server
    estensione VARCHAR(16) NOT NULL,
    IdTask INT, -- Nel caso in cui il file non sia associato a nessuna task, questo campo sarà nullo
    FOREIGN KEY (IdTask) REFERENCES Task(IdTask)
);

CREATE TABLE IF NOT EXISTS Messaggio (
    IdMessaggio INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    testo VARCHAR(255) DEFAULT "",
    visibilità BOOLEAN DEFAULT TRUE NOT NULL, -- TRUE lo intendo come messaggio pubblico.
    NomeUtente VARCHAR(32) NOT NULL, -- Obbligatoriamente il messaggio dovrà essere stato messo da qualcuno
    IdProgetto INT NOT NULL,
    FOREIGN KEY (NomeUtente) REFERENCES Utente(NomeUtente),
    FOREIGN KEY (IdProgetto) REFERENCES Progetto(IdProgetto)
);

CREATE TABLE IF NOT EXISTS UtentePartecipaTask (
    NomeUtente VARCHAR(32) NOT NULL,
    IdTask INT NOT NULL,
    valutazioneUtente INT CHECK (valutazioneUtente > 0 AND valutazioneUtente < 6 OR NULL), -- Controllo che la valutazione sia tra 1 e 5 oppure se non è stata inserita
    PRIMARY KEY (NomeUtente, IdTask),
    FOREIGN KEY (NomeUtente) REFERENCES Utente(NomeUtente),
    FOREIGN KEY (IdTask) REFERENCES Task(IdTask)
);

CREATE TABLE IF NOT EXISTS UtenteCaricaFile (
    NomeUtente VARCHAR(32) NOT NULL,
    IdFile INT NOT NULL,
    ultimaModifica TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    PRIMARY KEY (NomeUtente, IdFile),
    FOREIGN KEY (NomeUtente) REFERENCES Utente(NomeUtente),
    FOREIGN KEY (IdFile) REFERENCES File(IdFile)
);

CREATE TABLE IF NOT EXISTS UtentePossiedeSkill (
    NomeUtente VARCHAR(32) NOT NULL,
    NomeSkill VARCHAR(64) NOT NULL,
    livello INT CHECK (livello > 0 AND livello < 11) NOT NULL, -- Controllo che il livello sia tra 1 e 10
    PRIMARY KEY (NomeUtente, NomeSkill),
    FOREIGN KEY (NomeUtente) REFERENCES Utente(NomeUtente),
    FOREIGN KEY (NomeSkill) REFERENCES Skill(Nome)
);

CREATE TABLE IF NOT EXISTS UtentePartecipaProgetto (
    NomeUtente VARCHAR(32) NOT NULL,
    IdProgetto INT NOT NULL,
    PRIMARY KEY (NomeUtente, IdProgetto),
    FOREIGN KEY (NomeUtente) REFERENCES Utente(NomeUtente),
    FOREIGN KEY (IdProgetto) REFERENCES Progetto(IdProgetto)
);

CREATE TABLE IF NOT EXISTS SkillRichiesteInTask (
    NomeSkill VARCHAR(64) NOT NULL,
    IdTask INT NOT NULL,
    lvMinimo INT NOT NULL CHECK (lvMinimo > 0  AND lvMinimo < 11), -- livello di skill minimo richiesto per una task, controllo sia tra 1 e 10
    PRIMARY KEY (NomeSkill, IdTask),
    FOREIGN KEY (NomeSkill) REFERENCES Skill(Nome),
    FOREIGN KEY (IdTask) REFERENCES Task(IdTask)
);
