-- Creo utenti
INSERT INTO utente(NomeUtente, cognome, nome, cellulare, mail, password, ruolo) VALUES 
("AndreaRa13", "Rossi", "Andrea", "0000000000", "mail0@mail.it", SHA2("passwordInChiaro", 256), "Sviluppatore"),
("FishMan03", "Pesce", "Fabrizio", "0000000001", "mail1@mail.it", SHA2("passwordInChiaro", 256), "Sviluppatore"),
("Utente3", "Parco", "Merrotta", "0000000002", "mail2@mail.it", SHA2("passwordInChiaro", 256), "Grafico"),
("Utente4", "Buso", "Paolo", "0000000003", "mail3@mail.it", SHA2("passwordInChiaro", 256), "Grafico");

-- Inserisco i loro cv
INSERT INTO file(nome, descrizione, filePath, estensione) VALUES
("CV_AndreaRossi", "CV Andrea Rossi", "/CV/", "pdf"),
("CV_FabrizioPesce", "CV Fabrizio Pesce", "/CV/", "pdf"),
("CV_MerrottaParco", "CV Merrotta Parco", "/CV/", "pdf"),
("CV_PaoloBuso", "CV Paolo Buso", "/CV/", "pdf"); 

-- Assegno i cv
INSERT INTO utentecaricafile(NomeUtente, IdFile) VALUES
("AndreaRa13", 1),
("FishMan03", 2),
("Utente3", 3),
("Utente4", 4);

-- Creo delle skill
INSERT INTO skill(Nome) VALUES
("PROGRAMMAZIONE"),
("GRAFICA");
INSERT INTO skill(Nome, Padre) VALUES
("PROGRAMMAZIONE_C++", "PROGRAMMAZIONE"),
("PROGRAMMAZIONE_JAVA", "PROGRAMMAZIONE"),
("GRAFICA_CAD", "GRAFICA"),
("GRAFICA_BOOTSTRAP", "GRAFICA"),
("GRAFICA_BLENDER", "GRAFICA");

-- Assegno le skill
INSERT INTO utentepossiedeskill VALUES
("AndreaRa13", "PROGRAMMAZIONE_JAVA", 9),
("FishMan03", "PROGRAMMAZIONE_JAVA", 8),
("FishMan03", "PROGRAMMAZIONE_C++", 7),
("FishMan03", "GRAFICA_BOOTSTRAP", 5),
("Utente3", "GRAFICA_BOOTSTRAP", 8),
("Utente4", "PROGRAMMAZIONE_JAVA", 8),
("Utente4", "GRAFICA_BLENDER", 7);

-- Creo un progetto
INSERT INTO progetto(nome, descrizione, IdCoordinatore) VALUES
("Developers", "Creazione sito Developers", "AndreaRa13");

-- Creo delle task e le assegno al progetto 1
INSERT INTO task(dataInizio, dataFine, descrizione, IdProgetto) VALUES
("2024-04-22", "2024-05-22", "Progettazione backend", 1),
("2024-04-22", "2024-05-22", "Sviluppo grafica", 1);
INSERT INTO task(dataInizio, descrizione, IdProgetto) VALUES
("2024-05-22", "Manutenzione", 1); -- Può essere utile avere una task senza vincolo di fine per gestire la manutenzione.

INSERT INTO skillrichiesteintask VALUES 
("PROGRAMMAZIONE_JAVA", 1, 5),
("GRAFICA_BOOTSTRAP", 2, 7),
("PROGRAMMAZIONE_JAVA", 3, 8);

INSERT INTO utentepartecipatask(NomeUtente, IdTask) VALUES
("AndreaRa13", 1),
("FishMan03", 1),
("Utente3", 2),
("AndreaRa13", 3),
("Utente4", 3);

INSERT INTO utentepartecipaprogetto VALUES
("AndreaRa13", 1),
("FishMan03", 1),
("Utente3", 1),
("Utente4", 1);

INSERT INTO file(nome, descrizione, filePath, estensione, IdTask) VALUES
("Relazione progettazione backend", "Lorem Ipsum", "/Relazioni/", "PDF", 1);

INSERT INTO utentecaricafile(NomeUtente, IdFile, ultimaModifica) VALUES
("AndreaRa13", 5, "2024-04-23 10:00:00");
