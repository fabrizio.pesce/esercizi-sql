USE VINCOLI;

CREATE TABLE IF NOT EXISTS job2(
	job_id varchar(32),
    job_title varchar(32) default "" NOT NULL,
    min_salary decimal(10,2) default 8000 NOT NULL,
    max_salary decimal(10,2) default NULL,
    CHECK (max_salary < 25000),
    CONSTRAINT PK_Job PRIMARY KEY(job_id)
);
CREATE TABLE IF NOT EXISTS Reparto(
	IDReparto int AUTO_INCREMENT NOT NULL,
    nome varchar(32),
    CONSTRAINT PK_Reparto PRIMARY KEY(IDReparto)
);
CREATE TABLE IF NOT EXISTS Impiegato(
	IDImpiegato int AUTO_INCREMENT NOT NULL,
    nome varchar(32) NOT NULL,
    CF varchar(16) NOT NULL,
    job_id varchar(32) NOT NULL,
    IDReparto int NOT NULL,
    FOREIGN KEY (job_id) REFERENCES job2(job_id),
    FOREIGN KEY (IDReparto) REFERENCES reparto(IDReparto),
    CONSTRAINT PK_Impiegato PRIMARY KEY(IDImpiegato)
);
INSERT INTO job2(job_id, job_title) VALUES ("IT_YEET", "DROP DATABASE?");
INSERT INTO job2(job_id, job_title) VALUES ("IT_CONS", "NO MORE YEET");
INSERT INTO Reparto VALUES (30, "Reparto database");
INSERT INTO Impiegato VALUES (118, "Andrea", "YEET", "IT_YEET", 30);
UPDATE Impiegato SET job_id = "IT_CONS" WHERE IDImpiegato = 118 
										AND IDReparto = 30
										AND job_id NOT LIKE "SH%";

SELECT * FROM Impiegato WHERE IDImpiegato = 118 
							AND IDReparto = 30
							AND job_id NOT LIKE "SH%"; 
