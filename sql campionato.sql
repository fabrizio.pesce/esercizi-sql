Create database Campionati;
use campionati;
create table campionato(
	IDCampionato int AUTO_INCREMENT PRIMARY KEY,
    nome varchar(32),
    anno int
);
create table arbitro(
	CF varchar(16) PRIMARY KEY,
    nome varchar(32),
    cognome varchar(32)
);
create table luogo(
	IDLuogo int AUTO_INCREMENT PRIMARY KEY,
    nomeLuogo varchar(32),
    citta varchar(32)
);
create table giocatore(
	IDGiocatore int AUTO_INCREMENT PRIMARY KEY,
    nome varchar(32),
    cognome varchar(32),
    dataNascita date,
    luogoNascita varchar(32)
);
create table squadra(
	IDSquadra int AUTO_INCREMENT PRIMARY KEY,
    nomeSquadra varchar(32),
    citta varchar(32)
);
create table partita(
	IDPartita int AUTO_INCREMENT PRIMARY KEY,
    dataPartita date,
    IDCampionato int,
    IDLuogo int,
    IDSquadra1 int,
    IDSquadra2 int,
    puntiSquadra1 int,
    puntiSquadra2 int,
    FOREIGN KEY (IDCampionato) REFERENCES campionato(IDCampionato),
    FOREIGN KEY (IDLuogo) REFERENCES luogo(IDLuogo),
    FOREIGN KEY (IDSquadra1) REFERENCES squadra(IDSquadra),
    FOREIGN KEY (IDSquadra2) REFERENCES squadra(IDSquadra)    
);
create table direzione(
	CF varchar(16),
    IDPartita int,
    PRIMARY KEY(CF, IDPartita),
    FOREIGN KEY (CF) REFERENCES arbitro(CF),
    FOREIGN KEY (IDPartita) REFERENCES partita(IDPartita)       
);
create table formazione(
	IDGiocatore int,
    IDSquadra int,
    anno int,
    numero int,
	PRIMARY KEY(IDGiocatore, IDSquadra, anno),
    FOREIGN KEY (IDGiocatore) REFERENCES giocatore(IDGiocatore),
    FOREIGN KEY (IDSquadra) REFERENCES squadra(IDSquadra)
);
create table segna(
	IDGiocatore int,
    IDPartita int,
	minuto int,
    PRIMARY KEY(IDGiocatore, IDPartita, minuto),
    FOREIGN KEY (IDGiocatore) REFERENCES giocatore(IDGiocatore),
    FOREIGN KEY (IDPartita) REFERENCES partita(IDPartita)
);
alter table luogo add column regione varchar(32);
alter table luogo add column provincia varchar(2);
alter table squadra add column colore_squadra varchar(6); -- esadeciamale
alter table luogo modify column nomeLuogo varchar(255);