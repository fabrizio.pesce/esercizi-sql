/*Scrivi una query per trovare (first_name, last_name) e lo stipendio dei 
dipendenti che hanno uno stipendio più alto rispetto al dipendente il 
cui cognome='Bull'.»*/
USE ql;
SELECT FIRST_NAME, LAST_NAME, SALARY
FROM employees
WHERE SALARY > 
	(SELECT SALARY 
    FROM employees 
	WHERE LAST_NAME = "Bull");

/*Scrivi una query per (first_name, last_name) di tutti i dipendenti che 
lavorano nel reparto IT.
SELECT FIRST_NAME, LAST_NAME
FROM employees
WHERE JOB_ID Like "IT%";
*/
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID from employees
WHERE DEPARTMENT_ID IN 
	(SELECT DEPARTMENT_ID 
    FROM departments
	WHERE DEPARTMENT_NAME = "IT");
/*
Scrivi una query per trovare (first_name, last_name) dei dipendenti che 
hanno un manager e che hanno lavorato(dipendente) in un 
dipartimento con sede negli Stati Uniti.’
*/
SELECT FIRST_NAME, LAST_NAME
FROM employees
WHERE MANAGER_ID IS NOT NULL 
AND DEPARTMENT_ID IN
	(SELECT DEPARTMENT_ID 
	FROM departments 
    INNER JOIN locations 
    ON departments.LOCATION_ID = locations.LOCATION_ID
    WHERE locations.COUNTRY_ID = "US");

/*
SELECT first_name, last_name FROM employees 
WHERE manager_id in 
	(select employee_id FROM employees WHERE department_id IN 
		(SELECT department_id FROM departments WHERE location_id IN 
			(select location_id from locations where country_id='US')));
*/
/*
Scrivi una query per trovare (first_name, last_name) e lo stipendio dei 
dipendenti il cui stipendio è uguale allo stipendio minimo per il loro 
grado di lavoro.
*/
SELECT FIRST_NAME, LAST_NAME, SALARY
FROM employees
WHERE SALARY = 
	(SELECT MIN_SALARY 
    FROM jobs
    WHERE JOB_ID = employees.JOB_ID);
/*
Scrivi una query per trovare il nome (nome, cognome) e lo stipendio 
dei dipendenti che guadagnano uno stipendio superiore allo stipendio 
di tutti gli addetti alle spedizioni ('SH_CLERK'). Ordina i risultati dello 
stipendio dal più basso al più alto
*/
SELECT FIRST_NAME, LAST_NAME, SALARY
FROM employees
WHERE SALARY > 
	(SELECT MAX(SALARY) 
    FROM employees 
    WHERE JOB_ID = "SH_CLERK")
ORDER BY SALARY ASC;
/*
Scrivi una query per visualizzare l'ID dipendente, il nome, il cognome e 
i nomi dei reparti di tutti i dipendenti.
*/
SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME, 
	(SELECT DEPARTMENT_NAME 
    FROM departments 
    WHERE DEPARTMENT_ID = employees.DEPARTMENT_ID) AS "Nome dipartimento"
FROM employees;
/*
Scrivi una query per trovare (first_name, last_name) e lo stipendio dei 
dipendenti che guadagnano lo stesso stipendio del salario minimo per 
tutti i reparti
*/
SELECT FIRST_NAME, LAST_NAME, SALARY
FROM employees
WHERE SALARY = 
	(SELECT MIN(SALARY) 
    FROM employees 
    INNER JOIN departments 
    ON employees.DEPARTMENT_ID = departments.DEPARTMENT_ID)