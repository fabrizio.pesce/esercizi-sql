CREATE DATABASE IF NOT EXISTS E_SHOP;
USE E_SHOP;

CREATE TABLE IF NOT EXISTS Venditore(
	IDVenditore int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome varchar(16) NOT NULL,
    email varchar(32) NOT NULL,
    telefono varchar(10) NOT NULL,
    indirizzo varchar(127) NOT NULL,
    stato varchar(32) NOT NULL,
    citta varchar(32) NOT NULL,
    CAP varchar(5) NOT NULL,
    dataRegistrazione date NOT NULL
);

CREATE TABLE IF NOT EXISTS Prodotto(
	IDProdotto int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome varchar(32) NOT NULL,
    descrizione varchar(127) NOT NULL,
    prezzo decimal(8,2) NOT NULL,
    qta int NOT NULL,
    disponibile boolean NOT NULL,
    categoria varchar(127) NOT NULL,
    valutazioneMedia decimal(3,2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Acquirente(
	IDAcquirente int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    nome varchar(16) NOT NULL,
    email varchar(32) NOT NULL,
    telefono varchar(10) NOT NULL,
    indirizzo varchar(127) NOT NULL,
    stato varchar(32) NOT NULL,
    citta varchar(32) NOT NULL,
    CAP varchar(5) NOT NULL,
    dataRegistrazione date NOT NULL
);

CREATE TABLE IF NOT EXISTS Ordine(
	IDOrdine int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    IDAcquirente int NOT NULL,
    totale decimal(8,2) NOT NULL,
    dataOrdine date NOT NULL,
    stato varchar(32), -- stato ordine ig
    CONSTRAINT FK_Acquirente FOREIGN KEY (IDAcquirente) REFERENCES Acquirente(IDAcquirente)
);

CREATE TABLE IF NOT EXISTS DettaglioOrdine(
	IDDettaglio int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    IDOrdine int NOT NULL,
    IDProdotto int NOT NULL,
    qta int NOT NULL,
    prezzoUnitario decimal(8,2) NOT NULL,
    totaleRiga decimal(8,2) DEFAULT (qta*prezzoUnitario),
    CONSTRAINT FK_Ordine FOREIGN KEY (IDOrdine) REFERENCES Ordine(IDOrdine),
    CONSTRAINT FK_Prodotto FOREIGN KEY (IDProdotto) REFERENCES Prodotto(IDProdotto)
);

CREATE TABLE IF NOT EXISTS RecensioneProdotto(
	IDRecensione int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    IDProdotto int NOT NULL,
    IDAcquirente int NOT NULL,
    valutazione int NOT NULL,
    testoRecensione varchar(255),
    dataRecensione date NOT NULL,
    CONSTRAINT FK_Prodotto FOREIGN KEY (IDProdotto) REFERENCES Prodotto(IDProdotto),
    CONSTRAINT FK_Acquirente FOREIGN KEY (IDAcquirente) REFERENCES Acquirente(IDAcquirente)
);

CREATE TABLE IF NOT EXISTS RecensioneVenditore(
	IDRecensione int AUTO_INCREMENT NOT NULL PRIMARY KEY,
    IDVenditore int NOT NULL,
    IDAcquirente int NOT NULL,
    valutazione int NOT NULL,
    testoRecensione varchar(255),
    dataRecensione date NOT NULL,
    CONSTRAINT FK_Venditore FOREIGN KEY (IDVenditore) REFERENCES Venditore(IDVenditore),
    CONSTRAINT FK_Acquirente FOREIGN KEY (IDAcquirente) REFERENCES Acquirente(IDAcquirente)
);

INSERT INTO Venditore(nome, email, telefono, indirizzo, stato, citta, CAP, dataRegistrazione) VALUES
("Paolo", "paolo@gmail.com", "0000000000", "via roma", "Italia", "Roma", 00118, 2020/04/20),
("Simone", "simone@gmail.com", "1111111111", "via ferrara", "Italia", "Ferrara", 44122, 2021/04/20),
("Davide", "davide@gmail.com", "2222222222", "via ravenna", "Italia", "Ravenna", 48122, 2022/04/20),
("Marco", "marco@gmail.com", "33333333333", "via padova", "Italia", "Padova", 35128, 2023/04/20),
("Michele", "michele@gmail.com", "4444444444", "via bologna", "Italia", "Bologna", 40120, 2024/04/20);

INSERT INTO Prodotto(nome, descrizione, prezzo, qta, disponibile, categoria, valutazioneMedia) VALUES 
("Carte da gioco", "carte da gioco piemontesi", 8.99, 10, true, "Giochi", 4.4),
("Tastiera Corsair", "tastiera 70%", 99.99, 3, true, "Elettronica", 4.7),
("Arduino R3", "Terza versione di arduino", 25, 18, true, "Elettronica", 3.9),
("Raspberry pi4", "Quarta versione di raspberry pi", 89.99, 13, true, "Elettronica", 4),
("SSD Intenso 256GB", "SSD", 47.89, 3, true, "Elettronica", 2.2),
("Coperta emezon", "Coperta invernale", 23.98, 14, true, "Casa", 4.8),
("Candela", "Candela 20*15cm", 15.99, 9, true, "Casa", 3.6),
("Asciuga capelli", "Asciuga capelli da viaggio", 17.99, 0, false, "Casa", 3.4),
("Maglietta bianca", "Maglietta bianca taglia M", 8.99, 1, true, "Abbigliamento", 3.5),
("Zaino scuola", "Zaino scuola 400 tasche", 28.99, 4, true, "Scuola", 2);

INSERT INTO Acquirente (nome, email, telefono, indirizzo, stato, citta, CAP, dataRegistrazione) VALUES
('Mario', 'mario@example.com', '1234567890', 'Via Roma 10', 'Italia', 'Roma', '00100', '2024-01-01'),
('Luigi', 'luigi@example.com', '0987654321', 'Via Milano 20', 'Italia', 'Milano', '20100', '2024-01-02'),
('Peach', 'peach@example.com', '1122334455', 'Via Napoli 30', 'Italia', 'Napoli', '80100', '2024-01-03'),
('Daisy', 'daisy@example.com', '5566778899', 'Via Torino 40', 'Italia', 'Torino', '10100', '2024-01-04'),
('Yoshi', 'yoshi@example.com', '2233445566', 'Via Palermo 50', 'Italia', 'Palermo', '90100', '2024-01-05'),
('Toad', 'toad@example.com', '3344556677', 'Via Genova 60', 'Italia', 'Genova', '16100', '2024-01-06'),
('Wario', 'wario@example.com', '4455667788', 'Via Bologna 70', 'Italia', 'Bologna', '40100', '2024-01-07'),
('Waluigi', 'waluigi@example.com', '5566778899', 'Via Firenze 80', 'Italia', 'Firenze', '50100', '2024-01-08'),
('Bowser', 'bowser@example.com', '6677889900', 'Via Verona 90', 'Italia', 'Verona', '37100', '2024-01-09'),
('Donkey Kong', 'dk@example.com', '7788990011', 'Via Venezia 100', 'Italia', 'Venezia', '30100', '2024-01-10'),
('Diddy Kong', 'diddy@example.com', '8899001122', 'Via Padova 110', 'Italia', 'Padova', '35100', '2024-01-11'),
('Rosalina', 'rosalina@example.com', '9900112233', 'Via Bari 120', 'Italia', 'Bari', '70100', '2024-01-12'),
('Pauline', 'pauline@example.com', '0011223344', 'Via Catania 130', 'Italia', 'Catania', '95100', '2024-01-13'),
('Koopa Troopa', 'koopa@example.com', '1122334455', 'Via Parma 140', 'Italia', 'Parma', '43100', '2024-01-14'),
('Goomba', 'goomba@example.com', '2233445566', 'Via Modena 150', 'Italia', 'Modena', '41100', '2024-01-15');


