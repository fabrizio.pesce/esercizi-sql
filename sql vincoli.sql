/*
CREATE DATABASE IF NOT EXISTS VINCOLI;
USE VINCOLI;
CREATE TABLE IF NOT EXISTS job(
	job_id int AUTO_INCREMENT NOT NULL,
    job_title varchar(32) NOT NULL,
    min_salary decimal(10,2) NOT NULL,
    max_salary decimal(10,2) NOT NULL,
    CHECK (max_salary < 25000),
    CONSTRAINT PK_Job PRIMARY KEY(job_id)
);
INSERT INTO job(job_title, min_salary, max_salary) VALUES 
(
	"Ingegnere",
    1800,
    23000
);
*/
CREATE DATABASE IF NOT EXISTS VINCOLI;
USE VINCOLI;
CREATE TABLE IF NOT EXISTS job(
	job_id int AUTO_INCREMENT NOT NULL,
    job_title varchar(32) default "" NOT NULL,
    min_salary decimal(10,2) default 8000 NOT NULL,
    max_salary decimal(10,2) default NULL,
    CHECK (max_salary < 25000),
    CONSTRAINT PK_Job PRIMARY KEY(job_id)
);
INSERT INTO job VALUES 
();