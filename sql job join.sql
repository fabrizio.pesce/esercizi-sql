USE ql;

SELECT 
	FIRST_NAME AS Nome, 
    LAST_NAME AS Cognome, 
	jobs.JOB_TITLE AS Lavoro, 
    employees.DEPARTMENT_ID AS "ID reparto"  
FROM employees 
INNER JOIN jobs ON employees.JOB_ID = jobs.JOB_ID
INNER JOIN departments ON employees.DEPARTMENT_ID = departments.DEPARTMENT_ID
INNER JOIN locations ON departments.LOCATION_ID = locations.LOCATION_ID
WHERE locations.CITY = "London";

SELECT 
    dipendente.FIRST_NAME AS "Nome dipendente", 
    dipendente.LAST_NAME AS "Cognome dipendente",  
    manager.MANAGER_ID AS "ID manager", 
    manager.LAST_NAME AS "Cognome manager" 
FROM employees dipendente
INNER JOIN employees manager ON dipendente.MANAGER_ID = manager.EMPLOYEE_ID;

SELECT
	FIRST_NAME AS "Nome dipendente", 
    LAST_NAME AS "Cognome dipendente",
    HIRE_DATE AS "Data di assunzione",
    SALARY AS "Stipendio"
FROM employees
INNER JOIN departments ON employees.EMPLOYEE_ID = departments.MANAGER_ID
WHERE DATEDIFF(CURDATE(), HIRE_DATE)>15*365;

-- SELECT first_name, last_name, hire_date, salary, (DATEDIFF(now(), hire_date))/365 Experience FROM departments d JOIN employees e ON (d.manager_id = e.employee_id) WHERE (DATEDIFF(now(), hire_date))/365>15;

SELECT
	departments.LOCATION_ID AS "ID Luogo",
    STREET_ADDRESS AS "Indirizzo",
    CITY AS "Città",
    STATE_PROVINCE AS "STATO/PROVINCIA",
    countries.COUNTRY_NAME AS "NOME STATO"
FROM departments
INNER JOIN locations ON departments.LOCATION_ID = locations.LOCATION_ID
INNER JOIN countries ON locations.COUNTRY_ID = countries.COUNTRY_ID;

SELECT
	JOB_TITLE AS "Nome lavoro",
    SALARY AS "Stipendio"
FROM jobs
INNER JOIN job_history ON jobs.JOB_ID = job_history.JOB_ID
INNER JOIN employees ON job_history.EMPLOYEE_ID = employees.EMPLOYEE_ID
WHERE employees.SALARY>10000;

	

