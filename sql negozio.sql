/*
Esercizio provincie/regioni
use stato;
RENAME TABLE provincie TO Regioni;
ALTER TABLE Regioni ADD COLUMN `viaPrincipale` varchar(64);
*/

CREATE DATABASE Supermarket;
use Supermarket;

create table Reparto(
	NumeroReparto int PRIMARY KEY,
    nome varchar(32)
);
create table Personale(
	Matricola int AUTO_INCREMENT PRIMARY KEY,
    nome varchar(32),
    cognome varchar(32),
    mansione varchar(32),
    dataDiNascita date
);
create table Magazzino(
	NumeroScaffale int,
    Sezione varchar(32),
    qta int,	
    IDFornitore int,
    FOREIGN KEY (IDFornitore) REFERENCES Fornitore(IDFornitore),
    PRIMARY KEY(NumeroScaffale, Sezione)
);
create table Prodotto(
	CodiceProdotto varchar(16) PRIMARY KEY,
	nome varchar(32),
    descrizione varchar(256),
    prezzo decimal(6,2)
);
create table Cliente(
	CF varchar(16) PRIMARY KEY,
	nome varchar(32),
    cognome varchar(32),
    dataDiNascita date,
    IDTessera int,
    puntiTesserato int
);
create table personaleAccedeMagazzino(
	Matricola int,
    NumeroScaffale int,
    PRIMARY KEY(Matricola, NumeroScaffale),
    FOREIGN KEY (Matricola) REFERENCES Personale(Matricola),
    FOREIGN KEY (NumeroScaffale) REFERENCES Magazzino(NumeroScaffale)
);
create table personaleSistemaProdotto(
	Matricola int,
    CodiceProdotto varchar(16),
    PRIMARY KEY(Matricola, CodiceProdotto),
    FOREIGN KEY (Matricola) REFERENCES Personale(Matricola),
    FOREIGN KEY (CodiceProdotto) REFERENCES Prodotto(CodiceProdotto)
);
create table magazzinoContieneProdotto(
	CodiceProdotto varchar(16),
    NumeroScaffale int,
    PRIMARY KEY(CodiceProdotto, NumeroScaffale),
    FOREIGN KEY (CodiceProdotto) REFERENCES Prodotto(CodiceProdotto),
    FOREIGN KEY (NumeroScaffale) REFERENCES Magazzino(NumeroScaffale)
);
create table prodottoContenutoReparto(
	CodiceProdotto varchar(16),
    NumeroReparto int,
    PRIMARY KEY(CodiceProdotto, NumeroReparto),
    FOREIGN KEY (CodiceProdotto) REFERENCES Prodotto(CodiceProdotto),
    FOREIGN KEY (NumeroReparto) REFERENCES Reparto(NumeroReparto)
);
create table clienteAcquistaProdotto(
	CodiceProdotto varchar(16),
    CF varchar(16),
    PRIMARY KEY(CodiceProdotto, CF),
    FOREIGN KEY (CodiceProdotto) REFERENCES Prodotto(CodiceProdotto),
    FOREIGN KEY (CF) REFERENCES Cliente(CF)
);
ALTER TABLE Reparto ADD COLUMN codice int;
create table Fornitore(
	IDFornitore int AUTO_INCREMENT PRIMARY KEY,
    nome varchar(32),
    indirizzo varchar(255)
);

ALTER TABLE Fornitore drop column indirizzo;
