USE developers;

-- Lista di tutti i progetti attivi nel sistema.
SELECT progetto.IdProgetto AS ID, 
progetto.descrizione AS Progetto
FROM progetto
INNER JOIN task 
ON progetto.IdProgetto = task.IdProgetto
WHERE task.stato = true
GROUP BY progetto.IdProgetto;

-- Inserimento di un nuovo progetto e dei suoi task
INSERT INTO progetto(nome, descrizione, IdCoordinatore) VALUES
("TEST", "Creazione progetto TEST", "AndreaRa13");

INSERT INTO task(dataInizio, dataFine, descrizione, IdProgetto) VALUES
("2024-04-22", "2024-05-22", "TASK PROGRAMMAZIONE TEST", 2),
("2024-04-22", "2024-05-22", "TASK GRAFICA TEST", 2);

INSERT INTO skillrichiesteintask VALUES 
("PROGRAMMAZIONE_JAVA", 4, 5),
("GRAFICA_BOOTSTRAP", 5, 7);

INSERT INTO file(nome, descrizione, filePath, estensione, IdTask) VALUES
("Relazione TEST", "Lorem Ipsum", "/Relazioni/", "PDF", 4);

INSERT INTO utentecaricafile(NomeUtente, IdFile, ultimaModifica) VALUES
("AndreaRa13", 6, "2024-04-23 11:00:00");

INSERT INTO file(nome, descrizione, filePath, estensione, IdTask) VALUES
("Relazione TEST", "Lorem Ipsum", "/Relazioni/", "PDF", 4);

INSERT INTO utentecaricafile(NomeUtente, IdFile) VALUES
("AndreaRa13", 7);

-- Individuo i progetti più attivi
SELECT progetto.IdProgetto AS ID, 
progetto.descrizione AS Progetto,
MAX(utentecaricafile.ultimaModifica) AS UltimaModifica
FROM progetto
INNER JOIN task ON progetto.IdProgetto = task.IdProgetto
INNER JOIN file ON task.IdTask = file.IdTask
INNER JOIN utentecaricafile ON file.IdFile = utentecaricafile.IdFile
GROUP BY progetto.IdProgetto, progetto.descrizione
ORDER BY UltimaModifica DESC;

-- Individuo i progetti più stagnanti
SELECT progetto.IdProgetto AS ID, 
progetto.descrizione AS Progetto,
MAX(utentecaricafile.ultimaModifica) AS UltimaModifica
FROM progetto
INNER JOIN task ON progetto.IdProgetto = task.IdProgetto
INNER JOIN file ON task.IdTask = file.IdTask
INNER JOIN utentecaricafile ON file.IdFile = utentecaricafile.IdFile
GROUP BY progetto.IdProgetto, progetto.descrizione
HAVING UltimaModifica <= DATE_SUB(CURDATE(), INTERVAL 1 YEAR) 
ORDER BY ultimaModifica ASC;

-- Procedure sviluppatori
DELIMITER //
CREATE PROCEDURE GetDeveloper(IN skill1 VARCHAR(64), IN skill2 VARCHAR(64), IN lv1 INT, IN lv2 INT)
BEGIN

	SELECT utente.NomeUtente AS "Nome utente", 
    nome AS Nome,
    NomeSkill AS "Skill"
    FROM utente
    INNER JOIN utentepossiedeskill ON utente.NomeUtente = utentepossiedeskill.NomeUtente
    WHERE NomeSkill = skill1 AND livello >= lv1
    OR NomeSkill = skill2 AND livello >= lv2;
END//
DELIMITER ;

CALL GetDeveloper("PROGRAMMAZIONE_JAVA", "GRAFICA_BOOTSTRAP", 5, 5);

-- Ricerca progetti per keyword
DELIMITER //
CREATE PROCEDURE GetProject(IN keyWord VARCHAR(32))
BEGIN
	SELECT nome AS Nome,
    descrizione AS Descrizione
    FROM progetto
    WHERE nome LIKE CONCAT("%", keyWord, "%") OR
    descrizione LIKE CONCAT("%", keyWord, "%");
END//
DELIMITER ;

CALL GetProject("test");

-- Inserimento di un programmatore
DELIMITER //
CREATE PROCEDURE InsertNewDeveloper(IN username VARCHAR(32), IN cognomeNuovo VARCHAR(32), 
IN nomeNuovo VARCHAR(32), IN cellulareNuovo VARCHAR(10), 
IN mailNuovo VARCHAR(64), IN passwordNuovo VARCHAR(64), IN ruoloNuovo VARCHAR(64))
BEGIN
	INSERT INTO utente(NomeUtente, cognome, nome, cellulare, mail, password, ruolo) VALUES 
	(username, cognomeNuovo, nomeNuovo, cellulareNuovo, mailNuovo, SHA2(passwordNuovo, 256), ruoloNuovo);

	INSERT INTO file(nome, descrizione, filePath, estensione) VALUES
	(CONCAT("CV_",cognomeNuovo, nomeNuovo), CONCAT("CV ",cognomeNuovo, " ", nomeNuovo), "/CV/", "pdf"); 

	INSERT INTO utentecaricafile(NomeUtente, IdFile) VALUES
	(username, LAST_INSERT_ID());
    
END//
DELIMITER ;

CALL InsertNewDeveloper("Utente5", "Baolo", "Puso", "0000000004", "mail4@mail.it", "passwordInChiaro", "Sviluppatore");

-- Aggiornamento di una skill
DELIMITER //
CREATE PROCEDURE searchUserSkills(IN skill VARCHAR(32), IN username VARCHAR(32), OUT skillNumber INT)
BEGIN

	SELECT COUNT(*) INTO skillNumber
    FROM utentepossiedeskill
    WHERE NomeUtente = username AND
    NomeSkill = skill;
    
END//

CREATE PROCEDURE UpdateSkill(IN skill VARCHAR(32), IN username VARCHAR(32), IN lvl INT)
BEGIN
	DECLARE isAssigned INT DEFAULT 0;
    CALL searchUserSkills(skill, username, isAssigned);
    
    IF isAssigned = 0 THEN
		INSERT INTO utentepossiedeskill VALUES
        (username, skill, lvl);
	ELSE
		UPDATE utentepossiedeskill SET
        livello = lvl
        WHERE NomeUtente = username 
        AND NomeSkill = skill;
    END IF;
    
END//
DELIMITER ;

CALL UpdateSkill("PROGRAMMAZIONE_JAVA", "Utente5", 9);

-- Inserimento di una valutazione
DELIMITER //
CREATE PROCEDURE SetFeedback(IN feedback INT, IN username VARCHAR(32), IN idTask INT)
BEGIN
	 UPDATE utentepartecipatask
     SET valutazioneUtente = feedback
     WHERE NomeUtente = username AND
     IdTask = idTask;
END//
DELIMITER ;

CALL SetFeedback(5, "AndreaRa13", 1);

-- Creazione "Curriculum"
DELIMITER //
CREATE PROCEDURE CreateCurriculum(IN username VARCHAR(32))
BEGIN
	SELECT utente.NomeUtente AS "Nome utente",
    progetto.nome AS "Nome progetto",
    task.descrizione AS "Task",
    task.stato AS "Aperta?",
    utentepartecipatask.valutazioneUtente AS "Valutazione"
    FROM utente
    INNER JOIN utentepartecipatask ON utente.NomeUtente = utentepartecipatask.NomeUtente
    INNER JOIN utentepartecipaprogetto ON utente.NomeUtente = utentepartecipaprogetto.NomeUtente
    INNER JOIN task ON utentepartecipatask.IdTask = task.IdTask
    INNER JOIN progetto ON utentepartecipaprogetto.IdProgetto = progetto.IdProgetto
    WHERE utente.NomeUtente = username;
END//
DELIMITER ;

CALL CreateCurriculum("AndreaRa13");

-- Variazione delle date di un task
DELIMITER //
CREATE PROCEDURE UpdateTaskDate(IN currentTask INT, IN nuovaDataInizio DATE, IN nuovaDataFine DATE)
BEGIN
	IF nuovaDataFine IS NOT NULL THEN
		UPDATE task
		SET dataInizio = nuovaDataInizio,
		dataFine = nuovaDataFine
		WHERE IdTask = currentTask;
    ELSE
		UPDATE task
        SET dataInizio = nuovaDataInizio
        WHERE IdTask = currentTask;
    END IF;
END//
DELIMITER ;

CALL UpdateTaskDate(1, "2024-03-22", "2024-04-22");

-- Esclusione sviluppatore da una task
DELIMITER //
CREATE PROCEDURE RemoveDeveloperFromTask(IN currentTask INT, IN utente VARCHAR(32))
BEGIN
	DELETE FROM utentepartecipatask
    WHERE NomeUtente = utente 
    AND IdTask = currentTask;
END//
DELIMITER ;

CALL RemoveDeveloperFromTask(1, "FishMan03");

-- Visualizzazione History di un file
DELIMITER //
CREATE PROCEDURE GetFileHistory(IN nomeFile VARCHAR(127))
BEGIN
	SELECT utente.nome AS "Nome utente",
    utente.cognome AS "Cognome utente",
    utentecaricafile.ultimaModifica AS "Modifica"
    FROM utente
    INNER JOIN utentecaricafile ON utente.NomeUtente = utentecaricafile.NomeUtente
    INNER JOIN file ON utentecaricafile.IdFile = file.IdFile
    WHERE file.nome LIKE CONCAT(nomeFile, "%");
END//
DELIMITER ;

CALL GetFileHistory("Relazione TEST");