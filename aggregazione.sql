USE ql;
-- Eseguire una query che permetta di contare il salario degli impiegati
SELECT COUNT(SALARY) AS STIPENDI
FROM employees;
-- Eseguire una query che permetta di trovare il massimo e il minimo salario dalla tabella impiegati
SELECT MAX(SALARY) as "Stipendio massimo", 
	MIN(SALARY) as "Stipendio più basso" 
FROM employees;
-- Eseguire una query che permetta di trovare la media dei salari degli  impiegati e di contare il numero degli stessi.
SELECT AVG(SALARY) AS "Media stipendi" 
FROM employees;
-- Eseguire una query che permetta di trovare il numero di designazioni disponibili (Job_ID) nella tabella dei dipendenti
SELECT JOB_ID AS "Lavoro", COUNT(JOB_ID) AS "Numero lavoratori"
FROM employees
GROUP BY JOB_ID;