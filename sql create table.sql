/*
CREATE TABLE Provincie(
	Sigla varchar(2) PRIMARY KEY,
    nome varchar(64),
    numeroAbitanti int
);
*/

CREATE TABLE IF NOT EXISTS Provincie(
	Sigla varchar(2) PRIMARY KEY,
    nome varchar(64),
    numeroAbitanti int
);

CREATE TABLE IF NOT EXISTS Persone(
	IDPersona int,
    eta int,
    nome varchar(32),
    cognome varchar(32),
    sesso boolean
);

CREATE TABLE IF NOT EXISTS Job(
	id_impiegato int,
    data_inizio date,
    data_fine date,
    id_lavoro int PRIMARY KEY,
    id_dipartimento int
);

INSERT INTO Job VALUES(1, STR_TO_DATE("23/10/2022", "%d/%m/%Y"), STR_TO_DATE("23/12/2022", "%d/%m/%Y"), 1, 1);